package com.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;

@Controller
public class HomeController {
//	@RequestMapping("home")
//	public String home(@RequestParam("name") String name, HttpSession sesssion) {
//		// set the name to session
//		sesssion.setAttribute("name", name);
//
//		System.err.println("hi - " + name);
//
//		return "home";
//	}

//	@RequestMapping("home")
//	public ModelAndView home(
//		@RequestParam("name") String name,
//		ModelAndView mv
//	) {
//		mv.addObject("name", name);
//
//		mv.setViewName("home");
//
//		return mv;
//	}

	@RequestMapping("home")
	public ModelAndView home(
		Alien alien,
		ModelAndView mv
	) {
		mv.addObject("obj", alien);

		mv.setViewName("home");

		return mv;
	}
}
