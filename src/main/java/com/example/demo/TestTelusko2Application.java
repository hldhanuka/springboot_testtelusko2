package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestTelusko2Application {

	public static void main(String[] args) {
		SpringApplication.run(TestTelusko2Application.class, args);
	}

}
